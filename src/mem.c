#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}
 
static struct region alloc_region  ( void const * addr, size_t query ) {
 
    size_t actual_size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
    void *result = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);

    if (result == MAP_FAILED){
        result = map_pages(addr, actual_size, 0);
        if (result == MAP_FAILED) return REGION_INVALID;
    }


    struct region region = {
            .addr = result,
            .size = actual_size,
            .extends = addr == result
    };

    block_init(result, (block_size) {.bytes = actual_size,}, NULL);
    return region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return 0;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24
 
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(block==NULL|| !block_splittable(block,query)){
    return false;
  }
  const block_size next_size=(block_size) {.bytes=block->capacity.bytes-query};
  block->capacity.bytes=query;
  void *next_address=block_after(block);
  block_init(next_address,next_size,block->next);
  block->next=next_address;

  return true;
}
 

static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if((block==NULL)||(block->next==NULL)||(!mergeable(block,block->next))){
  return false;

  }
    block->capacity.bytes+=size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
    return true;

  

  
  
}
 
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t size) {
 struct block_header *prev = NULL;
    while (block) {
  while (try_merge_with_next(block)) {}

        if (block->is_free && block_is_big_enough(size, block)) {
            return (struct block_search_result) {
                .type = BSR_FOUND_GOOD_BLOCK,
                .block = block
            };
        }
  prev = block;
        block = block->next;
    }

    return (struct block_search_result) {
      .type = prev ? BSR_REACHED_END_NOT_FOUND : BSR_CORRUPTED, .block = prev};
}

 
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  
  struct block_search_result result=find_good_or_last(block,query);
  if(block==NULL){
    result.type=BSR_CORRUPTED;
    result.block=NULL;
    return result;
  }
  if(result.type==BSR_FOUND_GOOD_BLOCK){
    split_if_too_big(result.block,query);
    result.block->is_free=false;
    
    return result;
  }else{
    return result;
  }
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  /*  ??? */
    if (last == NULL){
      return NULL;
      }

    void* address = block_after(last);
    size_t size = size_max(query, BLOCK_MIN_CAPACITY);
    struct region region = alloc_region(address, size);

    if (region_is_invalid(&region)) {
      return NULL;
      }
      
    last->next = region.addr;

    if (try_merge_with_next(last)) return last;
    return region.addr;
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if(heap_start == NULL) return NULL;

  size_t alloc_size = size_max(BLOCK_MIN_CAPACITY, query);
  struct block_search_result result= try_memalloc_existing(alloc_size,heap_start);
  if(result.type==BSR_REACHED_END_NOT_FOUND){
    if(grow_heap(result.block,alloc_size)){
      try_merge_with_next(result.block);
      result=try_memalloc_existing(alloc_size,heap_start);

    }else{
      return NULL;
    }
  }
  if(result.block){
    result.block->is_free=false;
  }return result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
 
}
