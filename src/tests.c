#include "./tests.h"
#include "./mem.h"
#include "./mem_internals.h"
#include <stdio.h>
#include <stdlib.h>

struct block_header* block;
static void print_message(char* str,...){
    printf("___ %s ___\n",str);
    
}

static void print_error(char* str){
    print_message("ERROR: %s",str);
}


static void print_heap(char* str,void *const heap){
    print_message(str);
    debug_heap(stdout,heap);
}


static void delete_heap(void *heap,size_t sz){
    munmap(heap,size_from_capacity((block_capacity){.bytes=sz}).bytes);
}

static struct block_header* get_header(void* contents){
    return (struct block_header*) 
    ((uint8_t*) contents - offsetof(struct block_header, contents));
}

static int block_count(struct block_header* block){
    int count =0;
    struct block_header *iteration =block;
    while(iteration->next){
        if(!iteration->is_free){
            count+=1;
            iteration =iteration->next;
        }
    }
    return true;
}

void first_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~1~~~~~~");

    void* heap = heap_init(HEAP_SIZE);

    if(heap==NULL){
        print_error("Failed to initialize the heap");
        return;
    }print_heap("Heap has initialized",heap);

    void* test = _malloc(4096);
    if(test==NULL){
        print_error("Failed to allocate memory");
        return;
    }

    print_heap(heap, "After memory allocation");
    _free(test);
    delete_heap(heap,HEAP_SIZE);
    print_heap(heap,"Test 1 passed");
}




void second_test(){
    void* heap = heap_init(HEAP_SIZE);
    
    print_heap(heap,"~~~~~~TEST~~~~~~~~~~~~2~~~~~~");
    
    void* test_1=malloc(1024);
    print_message("Initializing 1-st block");
    void* test_2=malloc(2048);
    print_message("Initializing 2-nd block");
    print_message("Checking the number of blocks...");
    if(block_count(block)!=2){
        print_error("Number of blocks doesnt equel 2");

        print_error("Test 2 failed");
        _free(test_1);
        // print_message("Free 1-st block because of the error");
        _free(test_2);
        // print_message("Free 2-nd block because of the error");
        return;

    }else{
        _free(test_1);
        print_message("Free 1-st block");
        debug_heap(stdout, heap);
        if(block_count(block)==1){
            print_error("Test 2 failed");
            _free(test_2);
            return;
        }else{
            print_message("Test 2 passed");
        }
        _free(test_2);
    }

}
void third_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~3~~~~~~");

    void* heap = heap_init(4096);
    if(!heap){
        print_error("Heap is not initialized");
        return;
    }
    print_message("Heap is initialized");

    void *mem1=_malloc(1024);
    void *mem2=_malloc(1024);
    void *mem3=_malloc(1024);
     
    if(!mem1||!mem2||!mem3){
        print_error("Memory is not allocated");
        return;
    }
    print_message("Memory is allocated");


    _free(mem1);
    print_message("Free 1-st block");

    _free(mem3);
    print_message("Free 3-rd block");

    if(!mem2){
        print_error("Release of the 1-st damaged the second");
    }

    munmap(heap,size_from_capacity((block_capacity) {.bytes=4096}).bytes);
    print_message("Test 2 success");
}
void fourth_test(){

    print_message("~~~~~~TEST~~~~~~~~~~~~4~~~~~~");
    void* heap = heap_init(HEAP_SIZE);
    if (heap == NULL){
        print_error("Heap is not initialized");
        print_error("Test 4 failed");
        return;
    }

    print_message("Heap is initialized");

    debug_heap(stdout, heap);

    void *test1 = _malloc(64);
    void *test2 = _malloc(4056);
    
    if (!test1){
        print_error("Failed to initialize 1-st block");
        print_error("Test 4 failed");
        return;
    } print_message("1-st block is initialized");
    if(!test2){
        print_error("Failed to initialize 2-nd block");
        print_error("Test 4 failed");
        return;
    }print_message("2-nd block is initialized");

    struct block_header* header1 = get_header(test1);
    print_message("Getting header from 1-st block");
    struct block_header* header2 = get_header(test2);
    print_message("Getting header from 2-nd block");


    bool incorrect = (header1->capacity.bytes != 64) || (header1->next != test2) || (header2->capacity.bytes != 4056);

    if (incorrect){
        print_error("Failed to expand the region correctly");
        print_error("Test 4 failed");
        
        return;
    }

    _free(test1);

    print_message("Free 1-st block");

    debug_heap(stdout, heap);

    _free(test2);

    print_message("Free 2-nd block");

    debug_heap(stdout, heap);

    delete_heap(heap, HEAP_SIZE);
    print_message("Test 4 success");
}





 
void fifth_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~5~~~~~~");
    
    void *heap = heap_init(HEAP_SIZE);
    if (heap == NULL){
        print_error("Heap is not initialized");
        print_error("Test 5 failed");
        return;
    }print_message("Heap is initialized");

    void *block1 = _malloc(BLOCK_SIZE);
    print_message("1-st block is initialized");

    void *block2 = _malloc(BLOCK_SIZE);
    print_message("2-nd block is initialized");

    void *block3 = _malloc(BLOCK_SIZE);
    print_message("3-rd block is initialized");

    void *block4 = _malloc(BLOCK_SIZE);
    print_message("4-th block is initialized");

    void *block5 = _malloc(BLOCK_SIZE);
    print_message("5-th block is initialized");


    if(!block1||!block2||!block3||!block4||!block5){
        print_error("One of blocks is not initialized");
        print_error("Test 5 failed");
        return;

    }

    debug_heap(stdout, heap);

    _free(block4);
    print_message("4-th block is free");

    void *block6 = _malloc(BLOCK_SIZE);
    print_message("6-th block is initialized");

    if(!block6){
    print_message("6-th block is initialized");
    print_error("Test 5 failed");
        return;
    }


    struct block_header* heap_extended = (struct block_header*) heap;
    size_t heap_extended_size = size_from_capacity(heap_extended->capacity).bytes;

    if (heap_extended_size > HEAP_SIZE) {
        print_error("Test 5 failed");
        _free(block1);
        print_message("Free 1-st block because of the error");

        _free(block2);
        print_message("Free 2-nd block because of the error");

        _free(block3);
        print_message("Free 3-rd block because of the error");

        _free(block5);
        print_message("Free 5-th block because of the error");

        _free(block6);
        print_message("Free 6-th block because of the error");

        delete_heap(heap, HEAP_SIZE);
        print_message("Free heap because of the error");

        print_error("Test 5 failed");
        return;
    }

    debug_heap(stdout, heap);

    _free(block1);
    print_message("Free 1-st block");

    _free(block2);
    print_message("Free 2-nd block");

    _free(block3);
    print_message("Free 3-rd block");

    _free(block5);
    print_message("Free 5-th block");

    _free(block6);
    print_message("Free 6-th block");

    delete_heap(heap,HEAP_SIZE);
    print_message("Free heap");

    print_message("Test 5 success");


}

void run_all_tests(){
printf("~~~~~~STARTING~~~~~TESTS~~~~~~");

    first_test();

    second_test();

    third_test();

    fourth_test();

    fifth_test();

    printf("~~~~~TESTS~~~~~~~~~~~END~~~~~~");
}

